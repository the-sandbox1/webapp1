# Notes

<https://www.yogihosting.com/deploy-aspnet-core-app-kubernetes/>
<https://www.yogihosting.com/kubernetes-ingress-aspnet-core/>

## Kubernets Pods

- Pod can contain multiple containers

### Kubernets Objects:

Entities that describe the states of various components running on k8s cluster.
- What containerized applications are running on which worker nodes.
- The resources available to thos applications.
- THe policies such as restart policies, upgrades, and fault-tolerance.

#### Important Objects

1. Pods
2. Deployments
3. Services
4. Ingress

### Pods Networking and Data Sharing

- Pods can communicate with one another within the k8s cluster.
- Each pod is assigned it's own IP address
  - With this IP address they communicate with one another.

### Import commands for pods

See list of all the Pods running in a k8s cluster
```powershell
kubectl get pods
```

You can see a detailed description about your pods
```powershell
kubectl describe pod podname
```

### Deployments in kubernetes

Provies k8s about needed states for an app.

In deployment we describe things such as:
- The images to be used for the app.
- The container states needed for running the image.
- The number of Pods needed to run the containers and the way th epods should update or rollback.
- How to scale up and scale down.

### Deployment Configuration File

```yaml
apiVersion: apps/v1
kind: Deployment
# label used for delete commands etc.
metadata:
  name: first-dep
  labels:
    app: aspnet-core-app
spec:
  replicas: 1
  selector:
    matchLabels:
      component: web
  template:
    metadata:
      labels:
        component: web
    spec:
      containers:
        - name: csimpleweb
          image: simpleweb
          imagePullPolicy: Never
          ports:
            - containerPort: 80
```

## Kubernetes Services

A service assigns a unique IP address to the Pods so that they are exposed to the outside.
Service also gives a single DNS name for a set of Pods running an app.

Services connect Pods, especially when their IP addresses change. 

Does the following:
- A label selector that locates pods.
- Creates a clusterIP IP address that assigns port number and port definition to itself.
- Mapping of incoming ports to a targetPort of the Pods.

Services are of 4 types:
- **ClusterIP** - this is the default service type. This service is only reachable from within the cluster and not from outside.
- **NodePort** - this type of service is reachable from outside the cluster. The path for this service would be `<NodeIP>:<NodePort>`. I will be creating this type of service since I wan tmy app to be accessible on the browser with a url.
- **LoadBalancer** - this type of service is used in the cloud like Azure. The cloud provider provisioins on load balancer with a url.
- **ExternalName** - this service is mapped to a DNS name.

